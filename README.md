


SISTEME DISTRIBUITE
ASSIGNMENT 1

Request-Reply Communication Paradigm
Online Medication Platform






Student: Airoaei Catalin	
Grupa: 30242
Specializare: Calculatoare
Facultatea de Automatica si Calculatoare
Cuprins
1.	Specificarea problemei	3
1.1	Cerinte functionale	3
1.2	Cerinte non-functionale	3
1.3	Tehnologii de implementare	3
2.	Arhitectura conceptuala a sistemului distribuit	4
3.	Structura bazei de date	5
4.	Diagrama de deployment	6
5.	Bibliografie	6

 
1.	Specificarea problemei

Scopul acestei teme este de a proiecta si implementa un sistem distribuit cu trei nivele (Presention, Business si Data Access).  Aplicatia care trebuie dezvoltata consta intr-o platforma medicala si este alcatuita din doua aplicatii care comunica prin paradigm de comunicare Request-Reply. 

1.1	Cerinte functionale
•	Utilizatorii se pot loga in aplicatie. Acestia vor fi redirectionati pe pagina corespunzatoare rolului pe care il au.
•	Doctorii vor putea sa realizeze urmatoarele operatii:
o	Operatii CRUD asupra pacientiilor
o	Operatii CRUD asupra asistentiilor
o	Operatii CRUD asupra medicamentelor
o	Crearea unui plan de medicamentatie pentru un anumit pacient, care va consta dintr-o lista de medicamente, impreuna cu intervalele in care acestea trebuie luate si perioada pe care se va desfasura tratamentul
•	Asistentii vor putea:
o	Sa vizualizeze toti pacientii pentru care sunt responsabili
o	Sa vizualizeze planul de medicamentatie pentru fiecare pacient
•	Un pacient va putea:
o	Sa vizualizeze detaliile contului sau
o	Sa vizualizeze planul sau de medicamentatie
•	Utilizatorii care apartin unui anumit rol, nu vor putea accesa paginile corespunzatoare altor roluri

1.2	Cerinte non-functionale
•	Securitate: se foloseste autentificarea pentru a restriction accesul utilizatorilor la paginile care nu sunt asignate rolului lor (cookies, session, etc)

1.3	Tehnologii de implementare
•	Se folosesc tehnologiile:
o	Servicii REST pentru aplicatia de backend(Java Spring REST)
o	Framework bazat pe JavaScript(ReactJS)
•	Pentru operatiile cu baza de date MySQL se foloseste Hibernate ORM. Denumirea de ORM vine de la Object-Relational Mapping si este o tehnica de programare folosita pentru a converti date intre bazele de date relationale si limbaje de programare orientate pe obiecte cum ar fi Java sau C#. Hibernate mapeaza clasele din Java la tabele din baza de date, iar tipurile de date din Java sunt mapate la tipuri de date SQL. Beneficiul principal in utilizarea unui ORM este faptul ca programatorul nu mai este nevoit sa realizeze o mare parte din operatiile care sunt necesare pentru a realiza persistenta.




2.	Arhitectura conceptuala a sistemului distribuit


 


Dupa cum se poate observa si din diagram, aplicatia este structurata pet rei mari nivele:
•	Nivelul de prezentare: reprezentat de o aplicatie de frontend bazata pe framework-ul ReactJS bazat pe JavaScript.
•	Nivelul de business: reprezentate de aplicatia de backend ale carei principale componente sunt: Controllerele, Servicile, Repository-uri. Acestea comunica intre ele prin transmiterea de obiecte. Intre Repository si Servicii sunt obiectele de tip entitate, iar intre Servicii si Controllere cele de tip DTO(Data Transfer Object)
•	Nivelul de persistenta: baza de date relationala MySQL




3.	Structura bazei de date


 

Fiecare dinte aceste tabele este reprezentata in aplicatia Java ca o clasa, numita entitate. Dupa cum am mentionat anterior, maparea dintre aplicatia in Java si MySQL se realizeaza cu ajutorul Hibernate ORM.
La nivelul bazei de date exista constrangeri astfel incat, stergerea sa se poata realiza corepunzator, din toate tabelele din care e nevoie. De exemplu, la stergerea unui asistent se vor sterge toti pacientii asociati acestuia si contul sau de utilizator. Odata cu stergerea unui pacient se va sterge si planul sau de medicatie, iar odata cu acesta si toate intake-urile. Un alt caz il reprezinta stergerea unui medicament, caz in care se vor sterge si intake-urile in care acesta apare.





4.	Diagrama de deployment
 


5.	Bibliografie 
•	https://www.tutorialspoint.com/hibernate/hibernate_overview.htm
•	https://searchapparchitecture.techtarget.com/definition/RESTful-API
•	https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html








